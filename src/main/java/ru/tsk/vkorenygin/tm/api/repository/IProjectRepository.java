package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    int getSize();

    List<Project> findAll();

    List<Project> findAll(final Comparator<Project> comparator);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project changeStatusById(final String id, final Status status);

    Project changeStatusByName(final String name, final Status status);

    Project changeStatusByIndex(final Integer index, final Status status);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    void remove(Project project);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    void clear();

}
