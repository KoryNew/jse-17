package ru.tsk.vkorenygin.tm.command.task;

import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.enumerated.Sort;
import ru.tsk.vkorenygin.tm.model.Task;
import ru.tsk.vkorenygin.tm.util.DataUtil;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        List<Task> tasks;
        if (DataUtil.isEmpty(sort)) tasks = serviceLocator.getTaskService().findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(sortType.getComparator());
        }

        int index = 1;
        for (final Task task : tasks) {
            final String taskStatus = task.getStatus().getDisplayName();
            System.out.println(index + ". " + task + " (" + taskStatus + ")");
            index++;
        }
    }

}
