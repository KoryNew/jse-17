package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.ITaskService;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyDescriptionException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.model.Task;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(String name, String description) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (DataUtil.isEmpty(description))
            throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(Task task) {
        if (task == null)
            throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public boolean existsById(final String id) {
        if (DataUtil.isEmpty(id))
            return false;
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            return false;
        return taskRepository.existsByIndex(index);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null)
            return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findById(final String id) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(final String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (status == null)
            return null;
        return taskRepository.changeStatusById(id, status);
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (status == null)
            return null;
        return taskRepository.changeStatusByName(name, status);
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        if (status == null)
            return null;
        return taskRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Task startById(String id) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(String id) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.finishByName(name);
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (DataUtil.isEmpty(id))
            return null;
        if (DataUtil.isEmpty(name))
            return null;
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void remove(Task task) {
        if (task == null)
            throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
