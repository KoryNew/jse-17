package ru.tsk.vkorenygin.tm.util;

public interface DataUtil {

    static boolean isEmpty (final String value) {
        return (value == null || value.isEmpty());
    }

    static boolean isEmpty (final String[] values) {
        return (values == null || values.length == 0);
    }

    static boolean isEmpty (final Integer value) {
        return (value == null || value < 0);
    }

}
