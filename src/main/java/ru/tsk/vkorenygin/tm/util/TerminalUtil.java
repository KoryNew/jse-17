package ru.tsk.vkorenygin.tm.util;

import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static int nextNumber() {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (RuntimeException e) {
            throw new IncorrectIndexException(value);
        }
    }

}
